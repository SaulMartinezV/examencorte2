const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");
const { query } = require("express");


router.get('/',(req,res)=>{
    res.render('pagina.html',{titulo:'pagina',})
})
//LLamar los datos
router.get('/ExamenC2',(req,res)=>{
    const val = {
        recibo: req.query.recibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        tipo: req.query.tipo,  
        kilowatt:req.query.kilowatts,
        descontado:req.query.descuentado,
        descuento:req.query.descuento,
        total:req.query.total,
        subtotal:req.query.subtotal
    }
    res.render('pagina.html',val);
})


router.post('/ExamenC2',(req,res)=>{
    const val = {
        recibo: req.body.recibo,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        tipo: req.body.tipo,
        kilowatt:req.body.kilowatts,
        descontado:req.body.descuentado,
        descuento:req.body.descuento,
        total:req.body.total,
        subtotal:req.body.subtotal
    } 
    res.render('recibo.html',val);
})

//router
module.exports=router;