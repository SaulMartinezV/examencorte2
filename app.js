const http = require('http')
const express = require('express');
const bodyparser = require('body-parser');
const misRutas = require('./router/index');
const path = require('path');
//paquetes
const app = express();
app.set('view engine','ejs');
app.use(express.static(__dirname + '/public' ));
app.engine('html', require('ejs').renderFile);

app.use(bodyparser.urlencoded({extended:true}));
app.use(misRutas);

//Puertos
const puerto = 1004;
app.listen(puerto,()=>{
    console.log('Iniciado el puerto 1004');
})

//pagina de error
app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');

})
//Pruebas
//Publicacion